import { createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword } from "firebase/auth";

export const actionUserName = () => {
    return {type: 'CHANGE_USER',value:'MUHAMMAD ZAKIE'}
}

export const registerUserAPI = (data) => (dispatch) => {
    dispatch({type:'CHANGE_LOADING',value:true})
    return (
        createUserWithEmailAndPassword(getAuth(), data.email, data.password)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;
            console.log(user)
            // ...
            dispatch({type:'CHANGE_LOADING',value:false})
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode, errorMessage)
            // ..
            dispatch({type:'CHANGE_LOADING',value:false})
        })
    )
}

export const loginUserAPI = (data) => (dispatch) => {
    return new Promise((resolve,reject)=>{
        dispatch({type:'CHANGE_LOADING',value:true})
        signInWithEmailAndPassword(getAuth(), data.email, data.password)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;
            console.log("success : ",user)
            // ...
            const dataUser = {
                email: user.email,
                uid: user.uid,
                emailVerified: user.emailVerified
            }
            dispatch({type:'CHANGE_LOADING',value:false})
            dispatch({type:'CHANGE_ISLOGIN',value:true})
            dispatch({type:'CHANGE_USER',value:dataUser})
            resolve(true)
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode, errorMessage)
            // ..
            dispatch({type:'CHANGE_LOADING',value:false})
            dispatch({type:'CHANGE_ISLOGIN',value:false})
            reject(false)
        })
    })
}