import {BrowserRouter as Router, Routes, Link, Route} from 'react-router-dom'
import Dashboard from '../Dashboard';
import Login from '../Login';
import Register from '../Register';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../../config/redux'

export default function App() {
  return (
    <Provider store={store}>
      <Router>
      <div>
        <Routes>
          <Route path="/register" element={<Register/>}/>
          <Route path="/Login" element={<Login/>}/>
          <Route path="/" exact element={<Dashboard />}/>
        </Routes>
      </div>
      </Router>
    </Provider>
  );
}