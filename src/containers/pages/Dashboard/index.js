import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Dashboard extends Component {
  render() {
    return (
      <div>
        <p>Dashboard Page</p>
        <Link to='/register'>Go to register</Link>
        <Link to='/login'>Go to login</Link>
      </div>
    )
  }
}

export default Dashboard