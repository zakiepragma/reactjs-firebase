import React from 'react';
import ReactDOM from 'react-dom/client';
import firebase from './config/firebase';
import App from './containers/pages/App';

console.log(firebase)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);