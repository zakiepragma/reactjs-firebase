import React from "react"

const Button = ({onClick, title, loading}) => {

    if(loading==true){
        return <button className='btn disabled' disabled>Loading...</button>
    }
    return (
        <button onClick={onClick} className='btn'>{title}</button>
    )
}

export default Button;