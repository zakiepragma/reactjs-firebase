import React, { Component } from 'react'
import Button from '../../../components/atoms/Button';
import { connect } from 'react-redux';
import { registerUserAPI } from '../../../config/redux/action';

class Register extends Component {

    state = {
        email:'',
        password:'',
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
        })
    }

    handleRegisterSubmit = () => {
        console.log(this.state.email)
        console.log(this.state.password)

        let goto = true;
        if(this.state.email != '' && this.state.password != ''){
            goto = true;
        }else{
            goto = false;
        }
        if(goto){
            this.props.registerAPI({email:this.state.email, password:this.state.password})
            this.setState({
                email:'',
                password:''
            })
        }else{
            alert("email dan password wajib di isi")
        }
    }
    
  render() {
    return (
      <div className='auth-container'>
        <div className='auth-card'>
            <p className='auth-title'>Register Page</p>
            <input className='input' value={this.state.email} id='email' placeholder='e-mail' type='text' onChange={this.handleChange}/>
            <input className='input' value={this.state.password} id='password' placeholder='password' type='password' onChange={this.handleChange}/>
            <Button onClick={this.handleRegisterSubmit} title="Register" loading={this.props.isLoading}/>
        </div>
      </div>
    )
  }
}

const reduxState = (state) => ({
    isLoading: state.isLoading
})

const reduxDispatch = (dispatch) => ({
    registerAPI: (data) => dispatch(registerUserAPI(data))
})

export default connect(reduxState, reduxDispatch)(Register)