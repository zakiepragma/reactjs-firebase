import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '../../../components/atoms/Button'
import { loginUserAPI } from '../../../config/redux/action'

class Login extends Component {

  state = {
    email:'',
    password:'',
    login:'Login Page'
  }

  handleChange = (e) => {
      this.setState({
          [e.target.id]: e.target.value,
      })
  }

  handleLoginSubmit = async() => {
      console.log(this.state.email)
      console.log(this.state.password)
      let goto = true;
      if(this.state.email != '' && this.state.password != ''){
          goto = true;
      }else{
          goto = false;
      }
      // let navigate = useNavigate();
      if(goto){
          const res = await this.props.loginAPI({email:this.state.email, password:this.state.password}).catch(err=>err);
          if(res){
            console.log("Login success")
            this.setState({
              email:'',
              password:'',
              login: 'Anda berhasil login'
            });
            // return <Navigate to="/" replace/>
            // this.props.history.push('/')
            // return navigate('/')
            alert("Anda berhasil login")
          }else{
            alert("Login Gagal")
            console.log("Login gagal")
          }
          
      }else{
          alert("email dan password wajib di isi")
      }
  }

  render() {
    return (
      <div className='auth-container'>
        <div className='auth-card'>
            <p className='auth-title'>{this.state.login}</p>
            <input className='input' value={this.state.email} id='email' placeholder='e-mail' type='text' onChange={this.handleChange}/>
            <input className='input' value={this.state.password} id='password' placeholder='password' type='password' onChange={this.handleChange}/>
            <Button onClick={this.handleLoginSubmit} title="Login" loading={this.props.isLoading}/>
        </div>
      </div>
    )
  }
}

const reduxState = (state) => ({
  isLoading: state.isLoading
})

const reduxDispatch = (dispatch) => ({
  loginAPI: (data) => dispatch(loginUserAPI(data))
})

export default connect(reduxState,reduxDispatch)(Login)