// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";

// import "firebase/auth"
// import "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBrjvrAseh8lY2A-AnSpDH4Jr_pmIlbU_k",
  authDomain: "reactjs-firebase-99c2e.firebaseapp.com",
  projectId: "reactjs-firebase-99c2e",
  storageBucket: "reactjs-firebase-99c2e.appspot.com",
  messagingSenderId: "155575480024",
  appId: "1:155575480024:web:47622cd04a0ed78024f1ad",
  measurementId: "G-N056H0KG00"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export default firebase;